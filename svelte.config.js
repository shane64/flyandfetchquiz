import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-auto';
import path from 'path';

/** @type {import('@sveltejs/kit').Config} */
const config={
    kit: {
        adapter: adapter(),
        target: '#svelte',
        vite: {
            resolve: {
                alias: {
                    $utils: path.resolve('./src/utils')
                }
            }
        }
    },
    preprocess: [
        preprocess({
            postcss: true
        })
    ]
};

export default config;
