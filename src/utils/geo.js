function dist(p1, p2){
    const R=6371;

    const [lat1, lon1]=p1;
    const [lat2, lon2]=p2;

    const dLat=d2r(lat2-lat1);
    const dLon=d2r(lon2-lon1)
    const a=Math.sin(dLat/2)*Math.sin(dLat/2)+
          Math.cos(d2r(lat1))*Math.cos(d2r(lat2))*
          Math.sin(dLon/2)*Math.sin(dLon/2);
    const c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return R*c;
}

function d2r(a){
    return a*Math.PI/180;
}

export default {
    dist
};
