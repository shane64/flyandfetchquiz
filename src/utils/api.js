let config={
    baseURL: '',
    headers: {}
};

function init(_config){
    config={
        ...config,
        ..._config
    };
}

async function get(path, query){
    //Implement this function to interact
    //with the API
    return {};
}

export default {
    init,
    get
}
