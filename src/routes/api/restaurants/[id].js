const key='7cJEEAkbNn6HOtEGkMm1BkVXrN7UUIGnmiKWjUsk0AnoUgNtc3IsfHK9jIbEmYDzXXtmRFdNYV831D4zxpX1REiRM3ufmHcfGJbNOOpQOZFTXpoAYiW_uSAUV1KoYXYx';

async function get(req){
    const url=new URL('/v3/businesses/'+req.params.id, 'https://api.yelp.com');
    const resp=await fetch(url, {
        method: 'GET',
        headers: {
            authorization: 'Bearer '+key
        }
    });
    const json=await resp.json();
    delete(json.hours);
    return {
        status: 200,
        body: json
    };
}

export {get};
