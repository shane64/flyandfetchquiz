const key='7cJEEAkbNn6HOtEGkMm1BkVXrN7UUIGnmiKWjUsk0AnoUgNtc3IsfHK9jIbEmYDzXXtmRFdNYV831D4zxpX1REiRM3ufmHcfGJbNOOpQOZFTXpoAYiW_uSAUV1KoYXYx';

async function get(req){
    const url=new URL('/v3/businesses/search', 'https://api.yelp.com');
    url.search=new URLSearchParams({
        latitude: req.query.get('lat'),
        longitude: req.query.get('lon'),
        term: req.query.get('search') ?? '',
        radius: 40000,
        limit: 10,
        open_now: true,
        offset: req.query.get('skip') ?? 0
    });
    const resp=await fetch(url, {
        method: 'GET',
        headers: {
            authorization: 'Bearer '+key
        }
    });
    const json=await resp.json();
    const body={
        total: json.total,
        skip: req.query.get('skip') ?? 0,
        data: json.businesses.map(b=>{
            delete(b.distance);
            return b;
        })
    };
    return {
        status: 200,
        body
    };
}

export {get};
