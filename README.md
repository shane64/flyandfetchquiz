# Fly and Fetch coding quiz

Thanks for applying to Fly and fetch! This is a quiz just to see a bit of your ability to put together a small application and your coding style. Don't worry about treating it like a polished production application. It shouldn't take more than 2 to 3 hours to finish.

## Setup
To get started, make a **private** fork of this repository to do your work in. Treat it how you would any of your git repos, committing changes as normal. Once you have the project files, run `npm install` in the root directory. To run the project, use `npm run dev` or `npm run dev -- --open`. Edit this README with your answers to the questions at the bottom. Once you're happy with your work, invite shane@flyandfetch.com to your repository as a maintainer. If your Gitlab email is different from the email you used to apply, make a note of that in this README.

## Task
The objective is to use the provided API to show a user a list of nearby restaurants, and let them click through to a details page. Here are the specifics that we're looking for:
- Use the browser's [geolocation API](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API) to get the user's location
- Use that location to get a list of nearby restaurants from the API
- Display some basic restaurant information in a list, sorted by distance from the user
    - You can use the `/src/utils/geo.js` utility to get the distance between two `[lat, lon]` points
- Provide a text input where the user can add a search term
    - The results should update automatically when the user changes the search term
- Have a "load more" button to get the next page of results
    - Make sure the new list of results is still sorted by distance
- Let the user click on a restaurant to go to a details page `http://localhost:3000/:id`
- Display a bit more information about the restaurant on that page

We're looking for clean, functional code that would be easy to extend and maintain, as well as making sure any potential edge cases are handled.
Read the documentation for the libraries used, and try to follow recommended practices where possible.
If you feel the need to install additional libraries, add a section with the questions below describing what you added and why.
This is a quiz to evaluate coding ability, so the design doesn't need to be amazing. As long as it is functional that will be fine.

## API documentation
Base url: `https://localhost:3000/api`

### List restaurants
Endpoint: `/restaurants`
Method: `GET`
Parameters:
| Name | Type | Description |
| ---- | ---- | ----------- |
| lat | Number | **required.** Latitude to search by |
| lon | Number | **required.** Longitude to search by |
| search | String | *optional.* A general search term |
| skip | Number | *optional.* How many results to skip |

Returns:
```js
{
    total: Number, //how many results match the query in total
    skip: Number, //the provided skip parameter
    data: [Object] //a list of restaurant objects
}
```

### Get restaurant information
Endpoint: `/restaurants/:id`
Method: `GET`
Returns:
```js
{...} //the restaurant info
```

## External resources
[Svelte](https://svelte.dev/) and [SvelteKit](https://kit.svelte.dev/) are the frameworks used to build the UI.
[TailwindCSS](https://tailwindcss.com/) is set up to help speed up styling.

## Questions
The first set of results is loaded `onMount` on the home page. How can this be changed to improve the user experience? Describe how you might implement a change to achieve the improvement.

Given more time, and if this were a real product, what would you do to improve or change things? This is a very general question. Don't implement the improvements, just describe what you might do or steps you might take. This can be related to anything. For example code quality, project architecture, technologies used, user experience, performance, etc.
